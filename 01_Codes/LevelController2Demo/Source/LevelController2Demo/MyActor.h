/*
**  Copyright (c) 2016-2022 YeHaike(841660657@qq.com).
**  All rights reserved.
**  @ Date : 2017/11/21
*/

#pragma once

#include "GameFramework/Actor.h"
#include "MyActor.generated.h"

UCLASS()
class LEVELCONTROLLER2DEMO_API AMyActor : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AMyActor();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	
	
};
