/*
**  Copyright (c) 2016-2022 YeHaike(841660657@qq.com).
**  All rights reserved.
**  @ Date : 2017/11/21
*/

using UnrealBuildTool;
using System.Collections.Generic;

public class LevelController2DemoEditorTarget : TargetRules
{
	public LevelController2DemoEditorTarget(TargetInfo Target) : base(Target)
	{
		Type = TargetType.Editor;

        ExtraModuleNames.AddRange(new string[] { "LevelController2Demo" });
	}
}
